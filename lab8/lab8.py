import random
import test2 as t2
import time


n = 100000
n_stop = 1000000
n_step = 10000
search_radius = 0

try:
    search_radius = int(input('Введите радиус поиска = '))
except ValueError:
    print('Ошибка ввода')
    exit()

file = open('time.txt', 'w')

while n <= n_stop:

    time_start = time.time()

    lst_of_points1 = [random.uniform(-50, 50) for point in range(n)]
    lst_of_points2 = [random.uniform(-50, 50) for point in range(n)]
    point_center = (random.uniform(-50, 50), random.uniform(-50, 50))

    time_stop = time.time()
    time_different = str(time_stop - time_start).replace('.', ',')

    file.write(f'{time_different}\n')

    print(f'количество точек в заданном радиусе: {t2.counter_points(search_radius, lst_of_points1, lst_of_points2, point_center)}')

    n += n_step

file.close()

