import matplotlib.pyplot as plt
import math

#Функция, которая реализует основную программу
def mcode():
    x_lst = []
    y_lst = []
    try:
        a = float(input("Введите а --> "))
        x = float(input("Введите x --> "))
        x_max = float(input("Введите максимальное значение x "))
        command = int(input("Введите команду: G-1, F-2, Y-3. "))
        number = int(input("Введите количество шагов вычисления "))
        stride = (x_max-x)/number
    except ValueError:
        print("Некорректеный ввод")
        return 1

    step = 0

    for i in range(number):
        if command == 1:
            try:
                g = 8 * (21 * a ** 2 + 34 * a * x + 8 * x ** 2) / (2 * a ** 2 - 9 * a * x + 4 * x ** 2)
                x += stride
                step += 1
                print(step,"x=",x, "G=",g)
                x_lst.append(x)
                y_lst.append(g)
                if x >= x_max:
                    print("Достигнут максимум x")
                    break

            except ZeroDivisionError:
                g = None
                x += stride
                step += 1
                print(step, "x=", x, "G=", g)
                if x >= x_max:
                    print("Достигнут максимум x")
                    break

        elif command == 2:
            try:
                f = math.cosh(7 * a ** 2 + 52 * a * x + 21 * x ** 2)
                x += stride
                step += 1
                print(step,"x=",x, "F=",f)
                x_lst.append(x)
                y_lst.append(f)
                if x >= x_max:
                    print("Достигнут максимум x")
                    break

            except ValueError:
                f = None
                x += stride
                step += 1
                print(step, "x=", x, "F=", f)
                if x >= x_max:
                    print("Достигнут максимум x")
                    break

        elif command == 3:
            try:
                    y = math.log(-14 * a ** 2 - 13 * a * x + 10 * x ** 2 + 1)
                    x += stride
                    step += 1
                    print(step, "x=", x, "Y=", y)
                    x_lst.append(x)
                    y_lst.append(y)
                    if x >= x_max:
                        print("Достигнут максимум x")
                        break

            except ValueError:
                y = None
                x += stride
                step += 1
                print(step, "x=", x, "Y=", y)
                if x >= x_max:
                    print("Достигнут максимум x")
                    break
        else:
            print("Команда выбрана неправильно или не выбрана вообще")

    plt.xlabel('x ----->')
    plt.ylabel('f(x) ----->')
    plt.title('График функции')
    plt.plot(x_lst, y_lst, 'b-')
    plt.show()
    maxf = max(y_lst)
    minf = min(y_lst)
    print ('f(x) минимум = ',minf, 'f(x) максимум = ',maxf)
while True or OverflowError:
    mcode()
    again = input("Хотите начать программу сначала? [Y/N]: ")
    if again not in ["Y", "y", "Д", "д", "Да", "да"]:
        break