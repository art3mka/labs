import math

g_lst = []
f_lst = []
y_lst = []

try:
    data = list(map(float, input('Введите через пробел а,x,х максимальное,число шагов ').split()))
    a = int(data[0])
    x = int(data[1])
    x_max = int(data[2])
    number = int(data[3])
    stride = (x_max - x) / number

except ValueError:
    print("Ввод некорректен")

for i in range(number):
    try:
        g = (8 * (21 * a ** 2 + 34 * a * x + 8 * x ** 2)) / (2 * a ** 2 - 9 * a * x + 4 * x ** 2)
        g_lst.append(g)
        f = math.cosh(7 * a ** 2 + 52 * a * x + 21 * x ** 2)
        f_lst.append(f)
        y = math.log(-14 * a ** 2 - 13 * a * x + 10 * x ** 2 + 1)
        y_lst.append(y)
    except ZeroDivisionError:
        g = None
        g_lst.append(g)
        f = None
        f_lst.append(f)
    except ValueError:
        y = None
        y_lst.append(y)
    x += stride

file = open('arrays.txt', 'w')
file.write(f"{g_lst}\n{y_lst}\n{f_lst}")
file.close()

data = []

file = open('arrays.txt', 'r')
[data.append(line.split()) for line in file]
file.close()

for x in range(len(data[0])):
    print(f"G: f(x) = {data[0][x]}")
for x in range(len(data[1])):
    print(f"F: f(x) = {data[1][x]}")
for x in range(len(data[2])):
    print(f"Y: f(x) = {data[2][x]}")
