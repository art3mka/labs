import math


# Функция, реализующая основную программу
def mycode():
    y_lst = []

    try:
        data = list(map(float, input(
            'Введите через пробел а,x,х максимальное,число шагов ').split()))  # map - применение к каждому элементу
        a = int(data[0])
        x = int(data[1])
        x_max = int(data[2])
        number = int(data[3])
        search = input('Число каких символов в строке хотим найти? ')
        '''a = float(input("Введите а "))
        x = float(input("Введите x "))
        x_max = float(input("Введите x максимальное "))'''
        command = int(input("Введите команду 1-g, 2-f, 3-y. "))
        '''number = int(input("Количество шагов вычисления "))'''
        stride = (x_max - x) / number
    except ValueError:
        print("Ввод некорректен")
        return 1

    step = 0  # подсчет шагов

    for i in range(number):
        if command == 1:
            try:
                g = (8 * (21 * a ** 2 + 34 * a * x + 8 * x ** 2)) / (2 * a ** 2 - 9 * a * x + 4 * x ** 2)
                x += stride
                step += 1
                print(step,f'x={x:.1f},G={g:.1f}')
                y_lst.append(g)

            except ZeroDivisionError:
                g = None
                x += stride
                step += 1
                print(step, f'x={x:.1f},G={g:.1f}')

        elif command == 2:
            try:
                f = math.cosh(7 * a ** 2 + 52 * a * x + 21 * x ** 2)
                x += stride
                step += 1
                print(step, f'x={x:.1f},F={f:.1f}')
                y_lst.append(f)

            except ValueError:
                f = None
                x += stride
                step += 1
                print(step, f'x={x:.1f},F={f:.1f}')

        elif command == 3:
            try:
                y = math.log(-14 * a ** 2 - 13 * a * x + 10 * x ** 2 + 1)
                x += stride
                step += 1
                print(step, f'x={x:.1f},Y={y:.1f}')
                y_lst.append(y)
            except ValueError:
                y = None
                x += stride
                step += 1
                print(step, f'x={x:.1f},Y={y:.1f}')

        else:
            print("Комманда не выбрана или выбрана неправильно")

    y_lst = list(map(str, y_lst))
    print("Результаты - {0}".format(', '.join(y_lst)))

    count = 0
    for i in range(len(y_lst)):
        if y_lst[i] == search:
            count += 1
    print("Число подходящих элементов - {0}".format(count))


while True or OverflowError:
    mycode()
    again = input("Хотите начать программу сначала? [Y/N]: ")
    if again not in ["Y", "y", "Д", "д", "Да", "да"]:
        break
