import math

myfunction = input('Функция, которую хотите посчитать: ')
if myfunction in ['G', 'Y', 'F']:
    pass
else:
    exit('Неправильное название функции')

try:
    x = float(input('Введите x = '))
    a = float(input('Введите a = '))
except ValueError:
    exit('Value Error')

if myfunction == 'G':
    try:
        G=8 * (21 * a ** 2 + 34 * a * x + 8 * x ** 2) / ( 2 * a ** 2 - 9 * a * x + 4 * x ** 2)
    except ZeroDivisionError:
        exit('Zero Division Error')
    print('Function G  = ', G)

elif myfunction == 'F':
    try:
        F=math.cosh(7 * a ** 2 + 52 * a * x + 21 * x ** 2)
    except ValueError:
        exit('Value Error')
    print('Function F = ', F)
elif myfunction == 'Y':
    try:
        Y=math.log(-14 * a ** 2 - 13 * a * x + 10 * x ** 2 + 1)
    except ValueError:
        exit('Value Error')
    print('Function Y = ', Y)
