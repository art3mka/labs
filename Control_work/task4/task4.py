from random import randint
from time import time

from task_help import matrix_multiplication_calculation, calculation_trend_graph_visualize

# Максимальная размерность матрицы
dimension_max = 10**4
# Минимальная размерность матрицы
dimension_min = 10**2
# Шаг изменения размерности матрицы
dimension_step = 10**3

# Массив размерностей матрицы
dimensions = []
# Массив результирующих времен умножения матриц.
average_calculation_times = []
for dimension in range(dimension_min, dimension_max, dimension_step):
    # Заполняем первую матрицу i * j случайными значениями чисел от 1 до 10.
    matrix_a = [[randint(1, 10) for j in range(int(dimension**0.5))] for i in range(int(dimension**0.5))]
    # Вторая матрица - транспонированная первая
    matrix_b = zip(*matrix_a)

    # Массив результатов времени нескольких подсчетов.
    calculation_times = []
    for _ in range(3):
        timer_start = time()
        matrix_multiplication_calculation(matrix_a, matrix_b)
        timer_stop = time()
        calculation_times.append(timer_stop - timer_start)

    # Занесение значения подсчетов времени
    average_calculation_times.append(sum(calculation_times) / 3)
    # Занечение размерности матрицы
    dimensions.append(dimension)

# График
calculation_trend_graph_visualize(dimensions, average_calculation_times)
