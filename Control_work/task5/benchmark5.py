import time
import random
from task_help import gauss_jordan_calculation, cramer_calculation
import matplotlib.pyplot as plt
from numpy import poly1d as np_poly1d, polyfit as np_polyfit
from sys import getsizeof

time_lst_1 = []
time_lst_2 = []
n_lst = [] #размер матрицы
matrix_size_list = []

for rows_count in range(10, 100, 10):
    matrix = []
    for row in range(rows_count):
        time_temp = 0
        matrix_row = []
        for col in range(rows_count + 1):
            matrix_row.append(random.randint(1, 11))
        matrix.append(matrix_row.copy())
    matrix_size = getsizeof(matrix)
    matrix_size_list.append(matrix_size)

    for _ in range(3):
        start = time.time()
        result_vector = gauss_jordan_calculation(rows_count, matrix) #Время выполнения функции
        end = time.time()
        time_temp += end - start

    time_lst_1.append(time_temp / 3)
    print(time_lst_1[-1])
    n_lst.append(row)

    time_temp = 0
    for _ in range(3):
        start = time.time()
        r, m = rows_count, matrix.copy()
        result_vector = cramer_calculation(rows_count, matrix)
        end = time.time()
        time_temp += end - start

    time_lst_2.append(time_temp / 3)
    print(time_lst_2[-1])

#Построение трендов времени и объема

# Тренды времени
plt.figure(figsize=(10,6))

plt.subplot(211)
plt.plot(n_lst, time_lst_1, "yo")
trend_gauss = np_poly1d(np_polyfit(n_lst, time_lst_1, 3))
plt.plot(n_lst, trend_gauss(n_lst), c="yellow", label=f"Тренд времени вычисления уравнений методом Гаусса-Жордана ")

plt.legend()
plt.xlabel("Количество элементов в списке")
plt.ylabel("Время вычисления, с.")

plt.subplot(212)
plt.plot(n_lst, time_lst_2, "ro")
trend_cramer = np_poly1d(np_polyfit(n_lst, time_lst_2, 2))
plt.plot(n_lst, trend_cramer(n_lst), c="red", label=f"Тренд времени вычисления уравнений методом Крамера")

plt.legend()
plt.xlabel("Количество элементов в списке")
plt.ylabel("Время вычисления, с.")

plt.show()

# Тренд объема
plt.plot(n_lst, matrix_size_list, "r")
trend_size = np_poly1d(np_polyfit(n_lst, matrix_size_list, 1))
plt.plot(n_lst,trend_size(n_lst), "red", label=f"Тренд объема матрицы")

plt.legend()
plt.xlabel("Количество элементов в списке")
plt.ylabel("Объем матрицы, с.")

plt.show()

