from task_help import gauss_jordan_calculation, cramer_calculation


# Ввод количества уравнений в системе
rows_count = int(input('Введите количество уравнений в системе:\t'))

# Матрица будет включать в себя и систему уравнений, и свободные коэффициенты на конце каждой строки матрицы.
matrix = []
print("Заполните ячейки матрицы, включая свободные коэффициенты (вводятся последними в строке матрицы системы:")
for row in range(rows_count):
    matrix_row = []
    for col in range(rows_count + 1):
        matrix_row.append(float(input(f"Введите ненулевое значение ячейки matrix[{row}][{col}]:\t")))
    matrix.append(matrix_row.copy())

calculation_method_choice = input("Выберите метод решения системы уравнений - Gauss-Jordan (1) или Cramer (2):\t")
if calculation_method_choice == "1":
    result_vector = gauss_jordan_calculation(rows_count, matrix)
else:
    result_vector = cramer_calculation(rows_count, matrix)
for i in range(rows_count):
    print(f"X{i} = {result_vector[i]}", end='\t')
