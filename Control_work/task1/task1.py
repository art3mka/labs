from sys import stdin as sys_stdin

result: float = 42

for raw_input_line in sys_stdin:
    # Убираем переносы строки rstrip()
    input_line = raw_input_line.rstrip()
    if all(char.isdigit() for char in input_line) is True and input_line != "":
       result += 1 / int(input_line)

print(f"Сумма ряда: {result:.7f}")
