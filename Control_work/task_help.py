from copy import deepcopy
from matplotlib import pyplot
from numpy import poly1d as np_poly1d, polyfit as np_polyfit


# Алгоритм SAXPY

def saxpy_calculation(vector_x, vector_y, scalar_a):
    calculated_vector = [
        vector_x_coord * scalar_a + vector_y_coord
        for vector_x_coord, vector_y_coord in zip(vector_x, vector_y)
    ]
    return calculated_vector


def matrix_multiplication_calculation(matrix_a, matrix_b):
    matrix_b = list(zip(*matrix_b))
    return [
        [
            sum(x_coord * y_coord for x_coord, y_coord in zip(matrix_a_row, matrix_b_column))
            for matrix_b_column in matrix_b
        ]
        for matrix_a_row in matrix_a
    ]


def calculation_trend_graph_visualize(dimensions, average_calculation_times):

    # Отрисовка основных точек результирующих времен вычислений.
    pyplot.plot(dimensions, average_calculation_times, "o")
    # Отрисовка линии тренда.
    trend = np_poly1d(np_polyfit(dimensions, average_calculation_times, 3))
    pyplot.plot(dimensions, trend(dimensions), "b", c="red", label=f"Аппроксимирующий тренд вычислений")

    pyplot.legend()
    pyplot.xlabel("Размерность матриц")
    pyplot.ylabel("Время вычисления, с.")

    pyplot.show()


def gauss_jordan_calculation(rows_count, matrix):
    # Прямой ход метода Гаусса
    for i in range(rows_count):
        for j in range(rows_count):
            if i != j:
                ratio = matrix[j][i] / matrix[i][i]
                for k in range(rows_count+1):
                    # Заменяем
                    matrix[j][k] = matrix[j][k] - ratio * matrix[i][k]


    result_vector = []
    # Обратный ход метода Гаусса.
    for i in range(rows_count):
        vector_coord = round(matrix[i][rows_count] / matrix[i][i], 2)
        result_vector.append(vector_coord)

    return result_vector


def cramer_calculation(rows_count, matrix):
    # Вычисляем определитель матрицы.
    matrix_determinant = determinant_fast(matrix)
    # Если определитель матрицы имеет 0 значение, решить систему уравнений невозможно.
    if matrix_determinant == 0:
        raise ValueError

    result_vector = []
    # Подсчитываем определители в соответствии с количеством уравнений в системе.
    for i in range(rows_count):
        line = deepcopy(matrix)
        for j in range(rows_count):
            # Заменяем значения в матрице на свободные коэффициенты.
            line[j][i] = matrix[j][-1]
        # Вычисляем определитель матрицы со свободными коэффициентами и делим на определитель матрицы.
        line_determinant = round(determinant_fast(matrix=line) / matrix_determinant, 2)
        result_vector.append(line_determinant)

    return result_vector


def zeros_matrix(rows, cols):
    """
    Creates a matrix filled with zeros.
        :param rows: the number of rows the matrix should have
        :param cols: the number of columns the matrix should have
        :return: list of lists that form the matrix
    """
    matrix = []
    while len(matrix) < rows:
        matrix.append([])
        while len(matrix[-1]) < cols:
            matrix[-1].append(0.0)

    return matrix


def copy_matrix(matrix):
    """
    Creates and returns a copy of a matrix.
        :param matrix: The matrix to be copied
        :return: A copy of the given matrix
    """
    # Section 1: Get matrix dimensions
    rows = len(matrix)
    cols = len(matrix[0])

    # Section 2: Create a new matrix of zeros
    new_matrix = zeros_matrix(rows, cols)

    # Section 3: Copy values of matrix into the copy
    for i in range(rows):
        for j in range(cols):
            new_matrix[i][j] = matrix[i][j]

    return new_matrix


def determinant_fast(matrix):
    """
    Create an upper triangle matrix using row operations.
        Then product of diagonal elements is the determinant
        :param matrix: the matrix to find the determinant for
        :return: the determinant of the matrix
    """
    # Section 1: Establish n parameter and copy A
    n = len(matrix)
    new_matrix = copy_matrix(matrix)

    # Section 2: Row manipulate A into an upper triangle matrix
    for fd in range(n):  # fd stands for focus diagonal
        if new_matrix[fd][fd] == 0:
            new_matrix[fd][fd] = 1.0e-18  # Cheating by adding zero + ~zero
        for i in range(fd+1, n):  # skip row with fd in it.
            cr_scaler = new_matrix[i][fd] / new_matrix[fd][fd]  # cr stands for "current row".
            for j in range(n):  # cr - crScaler * fdRow, one element at a time.
                new_matrix[i][j] = new_matrix[i][j] - cr_scaler * new_matrix[fd][j]

    # Section 3: Once AM is in upper triangle form ...
    product = 1.0
    for i in range(n):
        product *= new_matrix[i][i]  # ... product of diagonals is determinant

    return product

