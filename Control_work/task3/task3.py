from task_help import saxpy_calculation
# Тут определяем тип данных
input_type_choice = input("Какого типа будут входные значения: int или float?\t").lower()
if "int" or "INT" or "Int" in input_type_choice:
    input_type = int
elif "float" or "FLOAT" or "Float" in input_type_choice:
    input_type = float
else:
    raise TypeError
# Вводим значение скаляра и векторов в соответствии с типом данных
scalar_a = input_type(input_type(input("Введите значение скаляра A:\t")))
vector_x = list(map(input_type, input("Введите координаты вектора X через пробел:\t").split()))
vector_y = list(map(input_type, input("Введите координаты вектора Y через пробел:\t").split()))

# Функцией ZIP связываем координаты векторов X и Y
calculated_vector = saxpy_calculation(vector_x, vector_y, scalar_a)

print(f"Результат вычислений алгоритма saxpy - {', '.join(calculated_vector)}")
