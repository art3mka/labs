# Импортируем из стандартных библиотек только тот функционал, который требуется.
# Импортируем модули для проверки системы и исполения команды очистки терминала.
from os import name as os_name, system as os_system
from time import sleep as time_sleep
from sys import stdin as sys_stdin

# Предварительная очистка экрана терминала (в зависимости от системы)
os_system("cls" if os_name == "nt" else "clear")

# Считываем исходный файл построчно в массив.
earth_frames = []  # Все кадры

earth_frame = []  # Один кадр
earth_frame_read_flag = False  # Флаг на считывания кадра.
for input_line_raw in sys_stdin:
    input_line = input_line_raw.rstrip("\n")
    # Если ``` встречается в строке - значит считывание кадра должно либо начаться, либо закончиться.
    if input_line.startswith("```") == 1:
        # Условие начала считывания.
        if earth_frame_read_flag is False:
            earth_frame_read_flag = True
        # Условие окончания считывания.
        else:
            # Добавление копии кадра в массив всех кадров.
            # Добавляется именно копия, потому что добавив сам массив мы поместим туда указатель на наш очищаемый массив
            earth_frames.append(earth_frame.copy())
            earth_frame_read_flag = False
            earth_frame = []

        continue

    # Добавление строки кадра.
    if earth_frame_read_flag is True:
        earth_frame.append(input_line)


try:
    while True:
        for earth_frame in earth_frames:
            for earth_frame_line in earth_frame:
                print(earth_frame_line)
                print("\033[36m", end="")
            # Задержка перед выводом следующего кадра.
            time_sleep(0.4)

            # Очистка экрана термина
            os_system("cls" if os_name == "nt" else "clear")
except KeyboardInterrupt:
    pass
